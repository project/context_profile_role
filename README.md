# Context Profile Role

This module provides a new block condition.

When viewing a user profile (view mode full), it allows you to check if the
profile has one of the selected roles.

Use case: displaying different blocks on user profiles based on their roles.


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

The module provides a new condition plugin, so it is possible to add a new
visibility condition on blocks placed using the native system or using the
Context module.

Example using the native block system:
1. Navigate to Administration > Extend and enable the module.
1. Navigate to Administration > Structure > Block layout and choose a block
   to edit.
1. There is now a `User Profile Role` tab in the `Visibility` field set.
1. Options include:
    - Roles
    - Select a user profile value: use `User profile from URL`
    - Negate the condition
