<?php

declare(strict_types=1);

namespace Drupal\context_profile_role\Plugin\Condition;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Core\Condition\Attribute\Condition;
use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\Context\EntityContextDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\user\RoleStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'User Profile Role' condition.
 */
#[Condition(
  id: 'user_profile_role',
  label: new TranslatableMarkup('User Profile Role'),
  context_definitions: [
    'user_profile' => new EntityContextDefinition(
      data_type: 'entity:user',
      label: new TranslatableMarkup('User Profile Role'),
    ),
  ]
)]
class UserProfileRole extends ConditionPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The user storage.
   *
   * @var \Drupal\user\RoleStorageInterface
   */
  protected RoleStorageInterface $roleStorage;

  /**
   * Constructor.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    /** @var \Drupal\user\RoleStorageInterface $role_storage */
    $role_storage = $entity_type_manager->getStorage('user_role');
    $this->roleStorage = $role_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $options = [];
    $user_roles = $this->roleStorage->loadMultiple();
    foreach ($user_roles as $role) {
      $options[$role->id()] = $role->label();
    }
    $form['roles'] = [
      '#title' => $this->t('Roles'),
      '#type' => 'checkboxes',
      '#options' => $options,
      '#default_value' => $this->configuration['roles'],
    ];
    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    /** @var array $form_state_roles_value */
    $form_state_roles_value = $form_state->getValue('roles');
    $this->configuration['roles'] = \array_filter($form_state_roles_value);
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function summary(): MarkupInterface {
    if (\count($this->configuration['roles']) > 1) {
      $roles = $this->configuration['roles'];
      $last = \array_pop($roles);
      $roles = \implode(', ', $roles);
      return $this->t(
        'The user role is @roles or @last',
        ['@roles' => $roles, '@last' => $last]
      );
    }
    $bundle = \reset($this->configuration['roles']);
    return $this->t('The user role is @bundle', ['@bundle' => $bundle]);
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    if (empty($this->configuration['roles']) && !$this->isNegated()) {
      return TRUE;
    }
    $user_profile_has_role = FALSE;
    /** @var \Drupal\user\UserInterface|null $user_profile */
    $user_profile = $this->getContextValue('user_profile');
    if ($user_profile) {
      $user_profile_roles = $user_profile->getRoles();
      // Search if the user profile has one of the authorized roles.
      foreach ($this->configuration['roles'] as $expected_role) {
        if (\in_array($expected_role, $user_profile_roles, TRUE)) {
          $user_profile_has_role = TRUE;
          break;
        }
      }
    }

    return $user_profile_has_role;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['roles' => []] + parent::defaultConfiguration();
  }

}
